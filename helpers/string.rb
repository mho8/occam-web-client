# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  module StringHelpers
    # These helper methods are for string manipulation or testing.
    # Right now, the only methods here are for internationalization support
    # for right-to-left languages. When we see a right-to-left script, we need
    # to style the page to reflect that.

    def direction(string)
      # Returns :rtl if the string starts with a rtl character
      #         :ltr otherwise
      if string.nil?
        return :ltr
      end

      @rtl_script_types  ||= ["Arabic", "Hebrew"]
      @rtl_script_regexp ||= "[#{@rtl_script_types.map do |script|
        "\\p{#{Regexp.escape(script)}}"
      end.join}]"

      if string.match(/^#{@rtl_script_regexp}/)
        :rtl
      else
        :ltr
      end
    end
  end

  helpers StringHelpers
end
