# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  class System
    require 'json'

    def self.local
      Occam::System.new
    end

    # Creates an instance that represents the System.
    def initialize(options = {})
      @account = options[:account]
    end

    def info
      cmdOptions = {}

      if @account
        cmdOptions["-T"] = @account.token
      end

      result = Occam::Worker.perform("system", "view", [], cmdOptions)
      JSON.parse(result[:data], :symbolize_names => true)
    end

    def hasComponent?(component)
      (self.info[:components] || []).include? component
    end
  end
end
