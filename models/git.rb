class Occam
  class Git
    def issue(object, action, *arguments)
      options = {}
      if arguments.length > 0 && arguments[-1].is_a?(Hash)
        options = arguments[-1]
        arguments = arguments[0...-1]
      end

      cmdArgs = [object.fullID, action, *arguments]
      cmdOptions = {}

      if object.account
        cmdOptions["-T"] = object.account.token
      end

      result = Occam::Worker.perform("git", "view", cmdArgs, cmdOptions, options[:stdin])
      result[:data]
    end
  end
end
