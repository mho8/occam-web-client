# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  get '/:uuid/HEAD' do
    # publishGitHead

    no_cache
    content_type "text/plain"

    # Send along the "publishGitHead" git action
    Occam::Git.new.issue(@object, "publishGitHead")
  end

  get '/:uuid/objects/pack/pack-*.*' do |prefix, suffix|
    if not ["pack", "idx"].include? suffix
      status 404
      return
    end

    if suffix == "idx"
      content_type "application/x-git-packed-objects-toc"
    else
      content_type "application/x-git-packed-objects"
    end

    forever_cache
    Occam::Git.new.issue(@object, "publishObjectsPack", prefix)
  end

  get '/:uuid/info/refs' do
    service = params["service"]

    if service
      content_type "application/x-#{service}-advertisement"
      Occam::Git.new.issue(@object, "publishUploadPack", service)
    else
      content_type "text/plain"
      Occam::Git.new.issue(@object, "publishInfoRefs")
    end
  end

  get '/:uuid/objects/info/*' do |file|
    puts "info #{file}"
  end

  get '/:uuid/objects/*/*' do |prefix, suffix|
    forever_cache
    content_type "application/x-git-loose-object"
    Occam::Git.new.issue(@object, "publishObjectsFile", prefix, suffix)
  end

  post '/:uuid/git-upload-pack' do
    if request.env["HTTP_CONTENT_ENCODING"] == "gzip"
      require 'zlib'
      gz = Zlib::GzipReader.new(request.body)
      input = gz.read()
    else
      input = request.body.read
    end

    content_type "application/x-git-upload-pack-result"

    stream do |out|
      ret = Occam::Git.new.issue(@object, "issueService", "upload-pack", :stdin => input)
      puts "read #{ret.length}"
      out << ret
      out.close
    end
  end
end
