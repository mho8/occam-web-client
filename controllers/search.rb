# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  get '/search' do
    format = request.preferred_type(['text/html', 'application/json', 'application/atom+xml', 'application/xml'])

    query = params["search"] || params["query"]

    objects = []
    types   = []

    if params["types"] == "on"
      types = Object.types(:query => query).map do |type|
        { :type => type }
      end
    end

    if params["objects"] == "on" || !params["type"].nil?
      objects = Object.search(:name => query, :type => params["type"])
    end

    case format
    when 'application/json'
      {
        "types" => types,
        "objects" => objects.map do |object|
          object.info.update(:revision => object.revision)
        end
      }.to_json
    when 'text/html'
      render :haml, :"search/results", :locals => {
        :types   => types,
        :objects => objects
      }
    end
  end
end
