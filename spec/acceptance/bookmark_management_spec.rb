require_relative "helper"

feature "Bookmark Management" do
  # You are are on the Object page
  background do
    @object = Occam::Object.create(:name => "foo", :type => "bar", :account => admin)
    @object.setPermission("read", "true")
    @object.setPermission("write", "true")

    visit @object.url
  end

  scenario "Bookmark an object while logged in" do
    login_as_user

    visit @object.url
    page.has_css?("h1 li.bookmark form input.bookmark").must_equal true
  end

  scenario "Bookmark an object while not logged in" do
    # There should not be a bookmark button
    page.has_no_css?("h1 li.bookmark form input.bookmark").must_equal true
  end

  scenario "Remove an existing bookmark while not logged in" do
    # There should not be a delete bookmark button
    page.has_no_css?("h1 li.bookmark_delete form input.bookmark_delete").must_equal true
  end

  scenario "Remove your own existing bookmark while logged in" do
    # There should be a delete bookmark button
    login_as_user

    visit @object.url
    page.has_css?("h1 li.bookmark_delete form input.bookmark_delete").must_equal true
  end

  scenario "Cannot remove a bookmark that belongs to someone else" do
    # There should not be a delete bookmark button
    page.has_no_css?("h1 li.bookmark_delete form input.bookmark_delete").must_equal true
  end
end
