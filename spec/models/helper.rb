require_relative "../helper"

silence_warnings do
  # Load a stubbing framework
  require "mocha"
  require "mocha/mini_test"
end

# A simple mocking function
def mock(class_or_name)
  ::Object.new
end

# Adds a parameter matcher that invokes a block to allow flexibility
class Resolves < Mocha::ParameterMatchers::Base
  def initialize(func)
    @func = func
  end

  def matches?(args)
    args = args.shift
    @func.call(args)
  end

  def mocha_inspect
    "resolves(#{@func})"
  end
end

def resolves(func)
  Resolves.new(func)
end

# Adds a parameter matcher to mocha that matches object ids inside arrays
class ObjectTokenMatches < Mocha::ParameterMatchers::Base
  def initialize(object)
    @object = object
    @type   = nil
  end

  def matches?(args)
    args = args.shift
    args.length > 0 and args[0].match(/#{Regexp.escape(@object.uuid)}(@#{Regexp.escape(@object.revision)})?(\/.*)?/)
  end

  def mocha_inspect
    "id_matches(#{@object.uuid})"
  end
end

def id_matches(object)
  ObjectTokenMatches.new(object)
end

class UnitTest < MiniTest::Spec
  def setup
    setup_base

    # Base case to ensure daemon is not connected to
    Occam::Daemon.any_instance.stubs(:connect)

    # Base case for daemon interaction
    Occam::Daemon
      .any_instance
      .stubs(:execute)
      .with(anything, anything, anything, anything, anything)
      .returns({
        :code => -1, :data => {
        }.to_json
      })
  end

  extend MiniTest::Spec::DSL

  register_spec_type(self) do |desc, *add|
    add.length == 0 || add[0][:type] != :feature
  end
end

# Person creation
def new_account(options = {})
  options[:username] = options[:username] || "name"
  options[:password] = options[:password] || uuid()

  person = new_person({:name => options[:username]}.update(options))

  token = options[:token] || uuid()

  # Stub token generation (error case)
  Occam::Daemon
    .any_instance
    .stubs(:execute)
    .with("accounts", "login",
          includes(options[:username]), Not(all_of(has_entry('-p', options[:password]), has_entry("-t", true))), anything).returns({
    :code => -1, :data => {
    }.to_json
  })

  # Stub token acceptance (error case)
  Occam::Daemon
    .any_instance
    .stubs(:execute)
    .with("accounts", "login",
          Not(includes(token)), all_of(has_entry('-a', true)), anything).returns({
    :code => -1, :data => {
    }.to_json
  })

  # Stub token generation
  Occam::Daemon
    .any_instance
    .stubs(:execute)
    .with("accounts", "login",
          includes(options[:username]), all_of(has_entry('-p', options[:password]), has_entry("-t", true)), anything).returns({
    :code => 0, :data => {
      :token => token,
      :roles => options[:roles] || [],
      :person => {
        :uuid => person.uuid,
        :id => person.uuid,
        :name => options[:username]
      }
    }.to_json
  })

  # Stub token acceptance
  Occam::Daemon
    .any_instance
    .stubs(:execute)
    .with("accounts", "login",
          includes(token), all_of(has_entry('-a', true)), anything).returns({
    :code => 0, :data => {
    }.to_json
  })

  Occam::Account.new(:token => token, :person_uuid => person.uuid)
end

def new_person(options = {})
  obj = new_object({:type => "person"}.update(options))
  obj.as(Occam::Person)
end

class FakeObject
  attr_reader :uuid
  attr_reader :revision

  def initialize(uuid, revision)
    @uuid = uuid
    @revision = revision
  end
end

# Object creation
def new_object(options = {})
  uuid     = options[:uuid]     || uuid()
  revision = options[:revision] || revision()

  fake = FakeObject.new(uuid, revision)

  # Establish all expected daemon responses for this object
  if options[:username]
    Occam::Daemon.any_instance.stubs(:execute).with("objects", "status", id_matches(fake), anything, anything).returns({
      :code => 0, :data => {
        :currentRevision => options[:currentRevision] || revision,
        :access => {
        },
        :account => {
          :username => options[:username],
          :roles => options[:roles]
        }
      }.to_json
    })
  else
    Occam::Daemon.any_instance.stubs(:execute).with("objects", "status", id_matches(fake), anything, anything).returns({
      :code => 0, :data => {
        :access => {
        },
        :currentRevision => options[:currentRevision] || revision
      }.to_json
    })
  end

  Occam::Daemon.any_instance.stubs(:execute).with("objects", "view", id_matches(fake), anything, anything).returns({
    :code => 0, :data => {
      :id => uuid,
      :name => options[:name] || "unnamed",
      :type => options[:type] || "object"
    }.update(options[:info] || {}).to_json
  })

  Occam::Object.new(:uuid => uuid, :revision => revision, :account => options[:account])
end

# Common Success/Failure Daemon Responses
def daemonSuccess
  {
    :code => 0, :data => {
    }.to_json
  }
end

def daemonFailure
  {
    :code => -1, :data => {
    }.to_json,
    :header => {
      :status => "error"
    }
  }
end

# Do not allow any daemon calls
#class Occam
#  class Daemon
#    remove_method :execute
#
#    # Ensure daemon is not called
#    def execute(*args)
#      {
#        :code => -1,
#        :header => {
#          :status => "error"
#        },
#        # TODO: probably shouldn't return valid data on error
#        :data => "{}"
#      }
#    end
#  end
#end
