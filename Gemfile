source 'https://rubygems.org'
ruby '>=2.4'

# Web Framework
gem 'sinatra', '~> 1.4.4'
gem 'sinatra-contrib'

# Markup Rendering Engine
gem 'haml', '~> 5.0.0'     # Haml
gem 'redcarpet' # Markdown

# Time duration markup (270s => "4 mins 30 secs")
gem 'chronic_duration'

# MIME Type Determination
gem 'mime-types', '>= 3.1.0'

# Internationalization
gem 'i18n'         # Main localization library
gem 'rails-i18n',  # Rails oriented default localizations
  :require => nil  #  (gives us default time/date localization)

# URL-Safe String Processing
gem 'stringex'

# Runs Rakefiles
gem 'rake'

# Testing environment libraries
group :test, :optional => true do
  gem 'capybara', '~> 2.16.1', :require => 'capybara/dsl'
  gem 'fabrication', '~> 1.2.0'
  gem 'rack-test', '~> 0.6.1', :require => 'rack/test'
  gem 'minitest', '~> 5.10.3', :require => 'minitest/autorun'
  gem "ansi"               # minitest colors
  gem "minitest-reporters" # minitest output
  gem "mocha", "~> 1.1.0"  # stubs
  gem 'simplecov', require: false # Code Coverage
  gem 'minitest-reporters-json_reporter' # minitest JSON reporter
  gem 'nokogiri' # Validates HTML generation

  # Javascript testing
  gem "jasmine"
end

# Web Server
gem 'puma'

# WebSocket support
gem "faye-websocket"

# Sass (Stylesheet Format)
gem 'sass'

# Encryption of Session Cookie
gem 'encrypted_cookie', git: 'https://github.com/cvonkleist/encrypted_cookie'

# Random Default Avatars
gem 'ruby_identicon'
group :avatarly, optional: true do
  gem 'avatarly'
end

# QR Code Generator
gem 'rqrcode'
