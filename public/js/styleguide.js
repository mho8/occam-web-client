function lines(selector, headers, data, attr) {
  var colors = ["steelblue", "green", "crimson"];

  if (!("width" in attr)) {
    attr.width = 800;
  }
  if (!("height" in attr)) {
    attr.height = 400;
  }
  if (!("margin" in attr)) {
    attr.margin = {left: 50, right: 10, bottom: 80, top: 10};
  }
  if (!("resizable" in attr)) {
    attr.resizable = false;
  }

  var chart_attr = {
    width:  attr.width+attr.margin.left+attr.margin.right,
    height: attr.height+attr.margin.top+attr.margin.bottom
  };

  var svg = d3.select(selector)
    .append("svg:svg")
    .attr("class", "chart")
    .data([data])
    .attr('preserveAspectRatio', 'xMidYMid')
    .attr('viewBox', '0 0 '+chart_attr.width+' '+chart_attr.height)
    .attr(chart_attr);

  var chart = svg.append("svg:g")
                 .attr('transform', 'translate('+attr.margin.left+', '+attr.margin.top+')');

  if (attr.resizable) {
    var aspect_ratio = chart_attr.width / chart_attr.height;
    var chart_dom = $(selector).find('svg');
    $(window).on("resize", function() {
      var width = chart_dom.parent().width();
      svg.attr("width", width);
      svg.attr("height", width / aspect_ratio);
    }).trigger("resize");
  }

  var chart_graphic = chart.append("svg:g")
                           .attr("class", "plot");

  chart_graphic.append("svg:clipPath")
    .attr("id", "clip-boundary")
    .append("rect")
      .attr("x", 0)
      .attr("y", 0)
      .attr("width", attr.width)
      .attr("height", attr.height);

  var lines,
      xAxis, yAxis;

  var x = d3.scale.linear()
            .domain([0, headers[0].length-1])
            .range([0, attr.width]);

  var y = d3.scale.linear()
            .domain( [0, d3.max( data, function( d ) { return d.conf_hi; } )] )
            .rangeRound( [0, attr.height] );

  // Data
  var line = d3.svg.line()
    .x(function(d) { return x(headers[0].indexOf(d.timepoint)) })
    .y(function(d) { return attr.height - y(d.fpkm) + .5 });

  var data_lines = headers[1].map(function(elem) {
    return data.filter(function(datapoint) {
      return datapoint.celltype == elem;
    });
  });

  // Confidence areas
  errorPolys = chart_graphic.append('g')
    .attr('class','errorpolys');

  errorPolys.selectAll('polygon')
    .data( data_lines )
    .enter().append('polygon')
    .attr("points", function(d) {
      return [
        d.map(function(elem) {
          return [x(headers[0].indexOf(elem.timepoint)) , attr.height - y(elem.conf_lo) + 0.5].join(",")
        }).join(" "),
        d.map(function(elem) {
          return [x(headers[0].indexOf(elem.timepoint)) , attr.height - y(elem.conf_hi) + 0.5].join(",")
        }).reverse().join(" ")
      ].join(" ")
    })
    .style({
      "fill": function(d,i){ return colors[i%3] },
      "opacity": "0.1",
      "stroke": "none"});

  // Lines
  lines = chart_graphic.append('g')
    .attr('class', 'lines');

  lines.selectAll('path')
    .data(data_lines)
    .enter().append('path')
    .style({
      fill: "none",
      "stroke-width": attr.width/100,
      stroke: function(d, i){return colors[i%3];}})
    .attr('d', line)
    .attr("clip-path", "url(#clip-boundary)");

  // Axis
  if ((attr.margin.left+attr.margin.right) > 10) {
    xAxis = d3.svg.axis()
      .scale(x)
      .ticks(headers[0].length)
      .tickSize(6, 3, 1)
      .tickFormat(function(tick) {
        return headers[0][tick]
      });

    yAxis = d3.svg.axis()
      .scale(d3.scale.linear().domain( [0, d3.max( data, function( d ) { return d.conf_hi; } )] ).rangeRound( [attr.height, 0] ))
      .tickSize(6, 3, 1)
      .orient('left');

    chart.append('g')
      .attr('class', 'x axis')
      .attr('transform', 'translate(0, ' + attr.height + ')')
      .call(xAxis);

    chart.append('g')
      .attr('class', 'y axis')
      .call(yAxis)
      .append("text")
      .style()
      .style({
        "text-anchor": "end"})
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .text("FPKM");

    chart.selectAll('.axis line')
      .style({
        "stroke": "black"});

    chart.selectAll('.tick line')
      .style({
        "stroke": "black"});

    var legend = chart.selectAll(".legend")
                      .data(headers[1])
                      .enter().append("g")
                      .attr("class", "legend")
                      .attr("transform", function(d, i) { return "translate(" + (((attr.width / 3) * i) + attr.margin.left + (attr.width/3*0.4)) + "," + (attr.height+attr.margin.top+attr.margin.bottom-40) + ")"; });

    legend.append("rect")
      .attr("transform", "translate(10,-9)")
      .attr("width", 18)
      .attr("height", 18)
      .style("fill", function(d,i){return colors[i]});

    legend.append("text")
      .attr("dy", ".35em")
      .style("text-anchor", "end")
      .text(function(d) { return d; });
  }
}

function bars(selector, headers, data, attr) {
  var colors = ["steelblue", "green", "crimson"];

  if (!("width" in attr)) {
    attr.width = 800;
  }
  if (!("height" in attr)) {
    attr.height = 400;
  }
  if (!("margin" in attr)) {
    attr.margin = {left: 50, right: 10, bottom: 80, top: 10};
  }
  if (!("resizable" in attr)) {
    attr.resizable = false;
  }

  var chart_attr = {
    width:  attr.width+attr.margin.left+attr.margin.right,
    height: attr.height+attr.margin.top+attr.margin.bottom
  };

  var rangeWidth = attr.width / 4;
  var barPadding = rangeWidth / 20;
  var barWidth = (attr.width - (barPadding * 11)) / 18;
  var barSpan = barWidth + barPadding;
  var rangeSpan = barWidth * 3 + barPadding * 2;

  var svg = d3.select(selector)
    .append("svg:svg")
    .attr("class", "chart")
    .data([data])
    .attr('preserveAspectRatio', 'xMidYMid')
    .attr('viewBox', '0 0 '+chart_attr.width+' '+chart_attr.height)
    .attr(chart_attr);

  var chart = svg.append("svg:g")
                 .attr('transform', 'translate('+attr.margin.left+', '+attr.margin.top+')');

  if (attr.resizable) {
    var aspect_ratio = chart_attr.width / chart_attr.height;
    var chart_dom = $(selector).find('svg');
    $(window).on("resize", function() {
      var width = chart_dom.parent().width();
      svg.attr("width", width);
      svg.attr("height", width / aspect_ratio);
    }).trigger("resize");
  }

  var x0 = d3.scale.ordinal()
             .domain(headers[0])
             .rangeBands([0, attr.width]);

  var x1 = d3.scale.ordinal()
             .domain(headers[1])
             .rangeBands([0, attr.width]);

  var y = d3.scale.linear()
            .domain( [0, d3.max( data, function( d ) { return d.conf_hi; } )] )
            .rangeRound( [0, attr.height] );

  // Bars
  var bars = chart.append('g')
                  .attr('class', 'bars');

  bars.selectAll( 'rect' )
    .data( data )
    .enter().append( 'rect' )
    .attr( 'x', function( d, i ) { return x0( d.timepoint ) + (rangeWidth/2) + (rangeSpan/2) - barWidth - (i%3) * barSpan - 0.5; } )
    .attr( 'y', function( d ) { return attr.height - y( d.fpkm ) + .5 } )
    .attr( 'width', barWidth)
    .attr( 'height', function( d ) { return y( d.fpkm ) } )
    .style({
      stroke: "white",
      fill:   function(d, i){return colors[i%3];}});

  // lines
  errorbars = chart.append('g')
    .attr('class','errorbars');
  topmarks = chart.append('g')
    .attr('class','errorbars');
  bottommarks = chart.append('g')
    .attr('class','errorbars');

  errorbars.selectAll('line')
    .data( data )
    .enter().append('line')
    .attr("class","errorbar")
    .attr( 'x1', function( d, i ) { return x0( d.timepoint ) + (rangeWidth/2) + (rangeSpan/2) - barWidth - (i%3) * barSpan - 0.5 + (barWidth / 2); } )
    .attr( 'x2', function( d, i ) { return x0( d.timepoint ) + (rangeWidth/2) + (rangeSpan/2) - barWidth - (i%3) * barSpan - 0.5 + (barWidth / 2); } )
    .attr( 'y1', function( d ) { return attr.height - y( d.conf_lo ) + .5 } )
    .attr( 'y2', function( d ) { return attr.height - y( d.conf_hi ) + .5 } )
    .style({
      "stroke": "black"});

  topmarks.selectAll('line')
    .data( data )
    .enter().append('line')
    .attr("class","errorbar")
    .attr( 'x1', function( d, i ) { return x0( d.timepoint ) + (rangeWidth/2) + (rangeSpan/2) - barWidth - (i%3) * barSpan - 0.5 + (barWidth / 4); } )
    .attr( 'x2', function( d, i ) { return x0( d.timepoint ) + (rangeWidth/2) + (rangeSpan/2) - barWidth - (i%3) * barSpan - 0.5 + (3 * barWidth / 4); } )
    .attr( 'y1', function( d ) { return attr.height - y( d.conf_hi ) + .5 } )
    .attr( 'y2', function( d ) { return attr.height - y( d.conf_hi ) + .5 } )
    .style({
      "stroke": "black"});

  bottommarks.selectAll('line')
    .data( data )
    .enter().append('line')
    .attr("class","errorbar")
    .attr( 'x1', function( d, i ) { return x0( d.timepoint ) + (rangeWidth/2) + (rangeSpan/2) - barWidth - (i%3) * barSpan - 0.5 + (barWidth / 4); } )
    .attr( 'x2', function( d, i ) { return x0( d.timepoint ) + (rangeWidth/2) + (rangeSpan/2) - barWidth - (i%3) * barSpan - 0.5 + (3 * barWidth / 4); } )
    .attr( 'y1', function( d ) { return attr.height - y( d.conf_lo ) + .5 } )
    .attr( 'y2', function( d ) { return attr.height - y( d.conf_lo ) + .5 } )
    .style({
      "stroke": "black"});


  // Axis
  var xAxis = d3.svg.axis()
    .scale(x0)
    .ticks(headers[0].length)
    .tickSize(6, 3, 1)
    .tickValues(headers[0]);

  var yAxis = d3.svg.axis()
    .scale(d3.scale.linear().domain( [0, d3.max( data, function( d ) { return d.conf_hi; } )] ).rangeRound( [attr.height, 0] ))
    .tickSize(6, 3, 1)
    .orient('left');

  chart.append('g')
    .attr('class', 'x axis')
    .attr('transform', 'translate(0, ' + attr.height + ')')
    .call(xAxis)

  chart.selectAll('.axis line')
    .style({
      "stroke": "black"});

  chart.append('g')
    .attr('class', 'y axis')
    .attr('transform', 'translate(' + x0.range()[0] + ')')
    .call(yAxis)
    .append("text")
    .style()
    .style({
      "text-anchor": "end"})
    .attr("transform", "rotate(-90)")
    .attr("y", 6)
    .attr("dy", ".71em")
    .text("FPKM");

  chart.selectAll('.tick line')
    .style({
      "stroke": "black"});

  var legend = chart.selectAll(".legend")
                    .data(headers[1])
                    .enter().append("g")
                    .attr("class", "legend")
                    .attr("transform", function(d, i) { return "translate(" + (((attr.width / 3) * i) + attr.margin.left + (attr.width/3*0.4)) + "," + (attr.height+attr.margin.top+attr.margin.bottom-40) + ")"; });

  legend.append("rect")
    .attr("transform", "translate(10,-9)")
    .attr("width", 18)
    .attr("height", 18)
    .style("fill", function(d,i){return colors[i]});

  legend.append("text")
    .attr("dy", ".35em")
    .style("text-anchor", "end")
    .text(function(d) { return d; });
}

$(document).ready(function(){
  var gene_data = [{"quant_status":"OK","sample_name":"E15_clang","_state":null,"conf_lo":96.047899999999998,"fpkm":125.35299999999999,"celltype":"clang","gene_id":"Fezf2","conf_hi":154.65799999999999,"timepoint":"E15"},{"quant_status":"OK","sample_name":"E15_msvc","_state":null,"conf_lo":50.048499999999997,"fpkm":65.894499999999994,"celltype":"msvc","gene_id":"Fezf2","conf_hi":81.740399999999994,"timepoint":"E15"},{"quant_status":"OK","sample_name":"E15_gcc","_state":null,"conf_lo":278.01499999999999,"fpkm":365.53800000000001,"celltype":"gcc","gene_id":"Fezf2","conf_hi":453.06,"timepoint":"E15"},{"quant_status":"OK","sample_name":"E16_clang","_state":null,"conf_lo":89.821100000000001,"fpkm":117.879,"celltype":"clang","gene_id":"Fezf2","conf_hi":145.93600000000001,"timepoint":"E16"},{"quant_status":"OK","sample_name":"E16_msvc","_state":null,"conf_lo":21.0473,"fpkm":28.1157,"celltype":"msvc","gene_id":"Fezf2","conf_hi":35.184199999999997,"timepoint":"E16"},{"quant_status":"OK","sample_name":"E16_gcc","_state":null,"conf_lo":142.416,"fpkm":186.523,"celltype":"gcc","gene_id":"Fezf2","conf_hi":230.62899999999999,"timepoint":"E16"},{"quant_status":"OK","sample_name":"E18_clang","_state":null,"conf_lo":62.9238,"fpkm":82.237799999999993,"celltype":"clang","gene_id":"Fezf2","conf_hi":101.55200000000001,"timepoint":"E18"},{"quant_status":"OK","sample_name":"E18_msvc","_state":null,"conf_lo":6.98949,"fpkm":9.8471799999999998,"celltype":"msvc","gene_id":"Fezf2","conf_hi":12.7049,"timepoint":"E18"},{"quant_status":"OK","sample_name":"E18_gcc","_state":null,"conf_lo":116.569,"fpkm":152.12100000000001,"celltype":"gcc","gene_id":"Fezf2","conf_hi":187.67400000000001,"timepoint":"E18"},{"quant_status":"OK","sample_name":"P1_clang","_state":null,"conf_lo":50.113,"fpkm":65.840199999999996,"celltype":"clang","gene_id":"Fezf2","conf_hi":81.567400000000006,"timepoint":"P1"},{"quant_status":"OK","sample_name":"P1_msvc","_state":null,"conf_lo":8.2547800000000002,"fpkm":11.4442,"celltype":"msvc","gene_id":"Fezf2","conf_hi":14.633699999999999,"timepoint":"P1"},{"quant_status":"OK","sample_name":"P1_gcc","_state":null,"conf_lo":97.344999999999999,"fpkm":126.78700000000001,"celltype":"gcc","gene_id":"Fezf2","conf_hi":156.22900000000001,"timepoint":"P1"}];

  var gene_headers = [
    ['E15','E16','E18','P1'],
    ['clang', 'msvc', 'gcc']
  ];

  bars('.graph.bars',   gene_headers, gene_data, { width: 600, height: 300 });
  lines('.graph.line',   gene_headers, gene_data, { width: 600, height: 300 });
});
