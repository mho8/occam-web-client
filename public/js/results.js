$(function() {
  var graph = $('#results-graph-wip.graph');

  // Load all graph options
  var graphSelect = $("<select></select>")
    .on('change', function(event) {
      type = $(this).children('option:selected').data('func');
      graph.html("");
      occam.graphs[type]($.extend(true, {}, options));
    });
  graph.before(graphSelect);

  options = {
    selector: "#results-graph-wip",
    data: {
      groups: []
    },
    width: 720
  }

  var type = "bars";

  // Figure out the width spanning to organize the results as a table

  // This spans out the keys (left-hand side) so that the values (right-hand side) line up
  $('#results-data ul').each(function() {
    // Determine the widest key
    var maxKeyWidth = Math.max.apply( Math, $(this).children('li').map(function() {
      return $(this).children('span.key').width()+1;
    }).get());

    // Apply that width to all keys whose values are not themselves an array.
    if (maxKeyWidth > 0) {
      $(this).children('li').each(function() {
        $(this).children('span.key').each(function() {
          if ($(this).parent().children('span.value').children('ul.array').length == 0) {
            $(this).css({
              display: "inline-block",
              width: ""+maxKeyWidth+"px",
            });
          }
        });
      });
    }
  });

  // The above lined up keys and values
  // Since we can have an arbitrary grid of keys and values, we then make sure every
  // key/value pair is the same size. That way, these elements will line up within a
  // grid.
  $('#results-data ul').each(function() {
    var maxWidth = Math.max.apply( Math, $(this).children('li').map(function() {
      return $(this).width()+1;
    }).get());

    $(this).children('li').each(function() {
      $(this).css({
        width: ""+maxWidth+"px",
      });
    });
  });

  // Graph Builder
  var menu = $('<div id="graph-builder-menu"><ul></ul></div>').css({
    display: 'none',
    position: 'absolute',
    "z-index": '999'
  }).on('mousedown', function(event) {
    event.stopPropagation();
  });

  menu.children('ul').append("<li id='graph-builder-menu-select'>Select</li>");
  menu.children('ul').append("<li id='graph-builder-menu-add-x'>Add X Series</li>");
  menu.children('ul').append("<li id='graph-builder-menu-label-x'>Label X</li>");

  $('body').on('mousedown', function(event) {
    menu.css({
      display: 'none'
    });
  }).append(menu);

  $(window).on('blur', function(event) {
    menu.css({
      display: 'none'
    });
  });

  var deselect = function(element) {
    var parent_ul = element.parent().parent();
    var grandparent_ul = parent_ul.parent().parent();

    var key = element.data('key');

    // Unhighlight all
    grandparent_ul.children('li.element')
                  .children('ul')
                  .children('li')
                  .children('span.key').each(function() {
      if ($(this).data('key') == key) {
        $(this).removeClass("in-results");
        $(this).css("color", "");
      }
    });
    element.css("color", "");
  };

  var select = function(element) {
    var parent_ul = element.parent().parent();
    var grandparent_ul = parent_ul.parent().parent();

    var key = element.data('key');

    if (grandparent_ul.hasClass("array")) {
      // Highlight all
      grandparent_ul.children('li.element')
                    .children('ul')
                    .children('li')
                    .children('span.key').each(function() {
        if ($(this).data('key') == key) {
          $(this).addClass("in-results");
          $(this).css("color", "red");
        }
      });
      element.css("color", "red");
    }
    else {
      element.css("color", "blue");
    }
  };

  var getValues = function(element) {
    var parent_ul = element.parent().parent();
    var grandparent_ul = parent_ul.parent().parent();

    var key = element.data('key');

    var ret = [];

    if (grandparent_ul.hasClass("array")) {
      // Highlight all
      grandparent_ul.children('li.element')
                    .children('ul')
                    .children('li')
                    .children('span.key').each(function() {
        if ($(this).data('key') == key) {
          ret.push(parseFloat($(this).parent().children('span.value').text()));
        }
      });
    }

    return ret;
  };

  var toggleSelect = function(element) {
    // Toggle class
    element.toggleClass("in-results");

    if (!element.hasClass("in-results")) {
      deselect(element);

      // Remove from graph
      options.data.groups.forEach(function(group, i) {
        if (group.key == element.data('key')) {
          options.data.groups.splice(i,1);
        }
      });

      graph.html("");
      occam.graphs[type]($.extend(true, {}, options));
    }
    else {
      select(element);

      // TODO: Keys as base64 values
      // TODO: Tag key here as its full path (foo.bar.baz instead of just baz)
      // TODO: Apply each of these two to the deselect as well
      options.data.groups.push({
        name: element.data('key'),
        key: element.data('key'),
        series: getValues(element)
      });

      options.labels = options.labels || {};
      options.labels.y = element.parent().children('span.value').data('units');

      graph.html("");
      occam.graphs[type]($.extend(true, {}, options));
    }
  };

  $('li span.key').each(function() {
    $(this).on('click', function(event) {
      // What key is this?
      // Do not open the context menu when someone is selecting text
      if (window.getSelection().rangeCount > 0 && !window.getSelection().getRangeAt(0).collapsed) {
        return;
      }

      // Open context menu
      menu.css({
        left: event.pageX,
        top:  event.pageY,
        display: 'block'
      });

      var element = $(this);

      menu.find('#graph-builder-menu-select')
          .unbind('click.toggleSelect')
          .on('click.toggleSelect', function(event) {
        toggleSelect(element);
        menu.css({
          display: 'none'
        });
      });

      event.preventDefault();
      event.stopPropagation();
    });
  });

  // Collapse Arrays
  $('#results-data ul.hash > li > span.expand').on('click', function(e) {
    $(this).toggleClass('shown');
    // Get associated array div
    if ($(this).hasClass('shown')) {
      $(this).parent().children('span.value').children('ul').css({
        display: 'block'
      });
      $(this).text("\u25be");
    }
    else {
      $(this).parent().children('span.value').children('ul').css({
        display: 'none'
      });
      $(this).text("\u25b8");
    }
  });

  // Create a '...' div for collapsed data points which upon clicking
  // will expand the data once more.
  fake_li = $('<li class="fake"><span class="key">...</span></li>').css({
    display: 'none'
  }).on('click', function(e) {
    $(this).parent().parent().children('span.expand').trigger('click');
  });

  $(this).find('#results-data ul.array > li.element > ul').append(fake_li);

  // Collapse array element hashes
  $('#results-data ul.array > li.element > span.expand').on('click', function(e) {
    $(this).toggleClass('shown');
    // Get associated array element div
    if ($(this).hasClass('shown')) {
      $(this).parent().children('ul').children('li').css({
        display: 'inline-block'
      });
      $(this).parent().children('ul').children('li.fake').css({
        display: 'none'
      });
      $(this).text("\u25be");
    }
    else {
      $(this).parent().children('ul').children('li').css({
        display: 'none'
      });
      $(this).parent().children('ul').children('li.fake').css({
        display: 'inline-block'
      });
      $(this).text("\u25b8");
    }
  });

  $('#results-data ul.hash > li > span.expand').trigger('click');
  $('#results-data > ul.hash > li > span.expand').trigger('click');
});
