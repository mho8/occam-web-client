/* This file handles the editing capabilities of the Paper/Pages sections.
 *
 * Paper has a bunch of "paper-widget" class items. Things such as paragraphs,
 * headers, subheaders, images, and 'widgets' which are embeddable js objects.
 *
 * These widgets can be modified when they exist and accessed within a workset.
 *
 * When they are modified, the changes are queued and sent upstream via
 * javascript. Since paper widgets are only viewable with javascript, there is
 * no current way to submit configuration changes outside of javascript.
 *
 * These changes are sent only when a configuration option hasn't been touched
 * within Paper.SAVE_DELAY_MS milliseconds, configurable below in the constants
 * section.
 */

// Initialize any Paper "pages" on the current page
$(function() {
  var pages = $('.content .tab-panel.page:not(.unloaded)');

  pages.each(function() {
    var page = new Occam.Paper($(this));
  });
});

var initOccamPaper = function(Occam) {
  /* Constructor
   */
  var Paper = Occam.Paper = function(page) {
    if (page.hasClass('unloaded')) {
      return;
    }

    this.page = page;

    // Apply events for edit/configure for Paper Pages
    this.applyTokenEvents(page.find('.line'));

    // Apply events for add component button
    this.applyAddEvent();

    // Apply events for dropdown selector
    this.applySelectorEvents();

    // Apply events to paragraphs
    this.applyParagraphEvents(page.find('p.paper-widget'));

    // Apply events to headers
    this.applyHeaderEvents(page.find('h2.paper-widget'));
    this.applyHeaderEvents(page.find('h3.paper-widget'));

    // Apply generic events
    this.applyItemEvents(page.find('.paper-widget'));

    // Apply events to widgets
    this.applyWidgetEvents(page.find('.card:not(.template) iframe.paper-widget'));

    // Pull out widget configurations
    this.pullConfigurations();
  };

  /* CONSTANTS */

  /* The time to wait after a configuration change to save the configuration
   * of a widget. (in milliseconds)
   */
  Paper.SAVE_DELAY_MS = 2000;

  /* METHODS */

  /* This method returns the number of page items on the current page.
   */
  Paper.prototype.pageItemCount = function() {
    return this.page.find(':not(.template) .paper-widget').length;
  };

  /* This method returns the page index for the current page.
   */
  Paper.prototype.pageIndex = function() {
    return this.page.index()
           - this.page.parent().children('.tab-panel.page').slice(0,1).index();
  };

  Paper.prototype.tryMergeCards = function(card) {
    var self = this;

    // Get a reference to each card
    var prevCard = card;
    var nextCard = card.next();
    if (nextCard.hasClass('separator') || nextCard.hasClass('widget-configuration')) {
      return;
    }

    // If this card was in-between two other cards, check for merges
    if (prevCard.length > 0 && nextCard.length > 0) {
      // Pull out the last item on prevCard and the first item on nextCard.
      var lastItem  = prevCard.children().slice(-1);
      var firstItem = nextCard.children().slice(1, 2);

      // If they should be merged, merge them
      if (self.shouldMerge(lastItem, firstItem)) {
        self.mergeCards(prevCard);
      }
    }

    // If we removed the last card on the page (nextCard doesn't exist) then
    // add a .line to the button to add a "add item" token.
    if (nextCard.length == 0) {
    }
  };

  Paper.prototype.removeCard = function(card) {
    // Get a reference to the previous and next cards
    var prevCard = card.prev();

    // Destroy the card. Feels good.
    card.remove();

    // Try to merge the cards
    self.tryMergeCards(prevCard);

    return this;
  };

  Paper.prototype.removeItem = function(item) {
    var self = this;

    var card = item.parents('.card');
    var prevCard = card.prev();

    // Remove widget configurations and separator //
    // Find and remove the iframe if not first
    var widget = false;
    if (item.next().length > 0) {
      if (item.next().prop("tagName").toLowerCase() == "iframe") {
        var tmp = item;
        item = item.next();
        tmp.remove();
      }
    }

    if (item.prop("tagName").toLowerCase() == "iframe") {
      // Remove separator and configuration cards
      var separator = item.parent().next();
      if (separator.hasClass('separator')) {
        separator.remove();
      }
      var configuration = item.parent().next();
      if (configuration.hasClass('widget-configuration')) {
        configuration.remove();
      }

      // Remove our own card
      card.remove();
    }

    // Remove new/configure button line
    item.prev().remove();

    // Remove item itself
    item.remove();

    /* Send remove message */
    var pageIndex = self.pageIndex();
    var itemIndex = item.data('item-index');
    Occam.object.queuePushSet("pages." + pageIndex + ".items." + itemIndex);

    // Re-adjust all other item indices (subtract one to them)
    self.page.find('.paper-widget').each(function() {
      var widget = $(this);
      var thisItemIndex = parseInt(widget.data('item-index'));
      if (thisItemIndex > itemIndex) {
        widget.data('item-index', thisItemIndex-1);
      }
    });

    self.tryMergeCards(prevCard);
  };

  Paper.prototype.cardIsEmpty = function(card) {
    return card.children(":not(.line)").length == 0;
  };

  /* Determines if the given elements, a and b, should be merged. That is,
   * they should not be separated by a card.
   *
   * Takes 'a' and 'b' which are jQuery element lists of length 1.
   *
   * Returns true if a and b belong together. false otherwise.
   */
  Paper.prototype.shouldMerge = function(a, b) {
    if (a.length == 0 || b.length == 0) {
      return true;
    }

    var tagA = a[0].tagName.toUpperCase();
    var tagB = b[0].tagName.toUpperCase();

    var aIsSimple = tagA == "P" || tagA == "H2" || tagA == "H3" || tagA == "IMG";
    var bIsSimple = tagB == "P" || tagB == "H2" || tagB == "H3" || tagB == "IMG";

    return aIsSimple && bIsSimple;
  };

  /* Merges the given card with the card that follows it.
   */
  Paper.prototype.mergeCards = function(card) {
    // Get a reference to the card we will merge with
    var nextCard = card.next();

    // Give up if the given card is the last card on the page.
    if (nextCard.length == 0) {
      return;
    }

    // Append the contents of the next card to the bottom of the given card.
    card.append(nextCard.children());

    // Destroy the empty card.
    nextCard.remove();
  };

  /* Splits the given card into two cards using the given splitElement as the
   * fulcrum to do so. The split element will be the first element of the new
   * card.
   *
   * Takes card, which is the '.card' and splitElement which is a child of that
   * card.
   *
   * Returns the handle to the repositioned splitElement.
   */
  Paper.prototype.splitCard = function(card, splitElement) {
    var self = this;

    var currentElementIndex = splitElement.index();

    var newCard = $('<div class="card filled page"></div>');

    if (currentElementIndex <= 0 || currentElementIndex >= card.children().length-1) {
      return;
    }

    newCard.append(card.children().slice(currentElementIndex));

    if (!self.cardIsEmpty(newCard)) {
      card.after(newCard);
    }

    return splitElement;
  };

  /* Generates and returns an unattached line section.
   *
   * When 'justAddItemButton' is true, the only token this line has
   * is an 'add-token' for situations where the line is at the bottom
   * of a card.
   */
  Paper.prototype.generateTokenLine = function(justAddItemButton) {
    var self = this;

    // Create an empty line
    var line = $('<div class="line"></div>');

    // We always add an add-item token
    line.append($('<div class="new-token"></div>'));

    // Sometimes we add a configure and delete token
    if (!justAddItemButton) {
      line.append($('<div class="delete-token"></div>'));
      line.append($('<div class="configure-token"></div>'));
    }

    // Apply events
    self.applyTokenEvents(line);

    // Return the formed line
    return line;
  };

  Paper.prototype.retrieveNewItemCard = function(page) {
    return page.find('.new.item').parent();
  };

  Paper.prototype.generateCard = function() {
    return $('<div class="card filled page"></div>');
  };

  /* Adds a New Item selector card where the given edit button is.
   *
   * Takes the new-item button element. It uses this to position the
   * new item section.
   *
   * Returns nothing.
   */
  Paper.prototype.addNewItemSection = function(newToken) {
    var self = this;

    // Show the New Item section
    var page = newToken.parents('.tab-panel');
    var newItemSection = self.retrieveNewItemCard(page);
    var prevCard = newItemSection.prev();

    // Detach newItemSection
    newItemSection.detach();

    // Try to merge the cards where the new item section previously was
    self.tryMergeCards(prevCard);

    newItemSection.attr('aria-hidden', 'false').hide();

    var itemIndex = newToken.parent().next().data('item-index');
    newItemSection.find('.button').data('item-index', itemIndex);

    var currentCard = newToken.parent().parent();

    // Move the New Item section to its new location
    if (newToken.parent().index() == 0) {
      // We are at the top
      currentCard.before(newItemSection);
    }
    else {
      // Divide existing card
      self.splitCard(currentCard, newToken.parent());
      currentCard.after(newItemSection);
    }

    newItemSection.slideDown(200);
    self.applySelectorEvents();
  };

  /* Applies events to the edit and configure buttons next to page items.
   *
   * Takes the '.line' elements to search for the buttons to attach the
   * events to.
   *
   * Returns nothing.
   */
  Paper.prototype.applyTokenEvents = function(elements) {
    var self = this;
    elements.children('.new-token').on('mouseenter', function(event) {
      $(this).parent().addClass('hovered');
    }).on('mouseleave', function(event) {
      $(this).parent().removeClass('hovered');
    }).on('click', function(event) {
      // Trigger animation to set color back to normal
      $(this).trigger('mouseleave');

      self.addNewItemSection($(this));
    });

    elements.children('.delete-token').on('click', function(event) {
      var item = $(this).parent().next();
      self.removeItem(item);
    });
  };

  Paper.prototype.addComponentAfter = function(type, itemIndex, card, afterItem, initialData) {
    var self = this;

    var line = self.generateTokenLine();
    if (afterItem === undefined || afterItem.length == 0) {
      card.append(line);
    }
    else {
      afterItem.after(line);
    }

    var data = {};
    var item = $('<p></p>');

    if (type == "paragraph") {
      item = self.page.find('.paragraph-item.template p').clone();
      line.after(item);
      data = {
        "content": item.text().trim()
      };
      if (initialData !== undefined) {
        data = {
          "content": initialData
        };
        item.text(initialData);
      }
      self.applyParagraphEvents(item);
    }
    else if (type == "header") {
      item = $("<h2>Header</h2>");
      line.after(item);
      data = {
        "content": item.text().trim()
      };
      self.applyHeaderEvents(item);
    }
    else if (type == "subheader") {
      item = $("<h3>Subheader</h3>");
      line.after(item);
      data = {
        "content": item.text().trim()
      };
      self.applyHeaderEvents(item);
    }
    else if (type == "image") {
      item = $('<img src="/images/paper/image_default.png">');
      line.after(item);
    }
    else if (type == "widget") {
      item = self.page.find('.card.widget-item.template').children().clone();
      line.after(item);
      self.initializeWidget(item);
      self.applyWidgetEvents(item.parent().children('iframe'));
      data = {};
    }

    // Re-adjust all other item indices (add one to them)
    self.page.find('.paper-widget').each(function() {
      var widget = $(this);
      var thisItemIndex = parseInt(widget.data('item-index'));
      if (thisItemIndex >= itemIndex) {
        widget.data('item-index', thisItemIndex+1);
      }
    });

    item.addClass('paper-widget');
    item.data('item-index', itemIndex);

    self.applyItemEvents(item);

    // Send append request
    var pageIndex = self.pageIndex();
    Occam.object.queuePushAppend('pages.' + pageIndex + '.items', {
      'type': type,
      'data': data
    }, itemIndex);
  }

  Paper.prototype.addComponent = function(type, itemIndex, page, afterCard) {
    var self = this;

    // Form new card based on type
    var card = self.generateCard();

    // Add card after given card
    if (afterCard !== undefined && afterCard.length > 0) {
      afterCard.after(card);
    }
    else {
      page.prepend(card);
    }

    self.addComponentAfter(type, itemIndex, card);

    // Try-merge the given card
    self.tryMergeCards(card);
    self.tryMergeCards(afterCard);
  };

  Paper.prototype.applyItemEvents = function(elements) {
    elements.each(function() {
      var item = $(this);
      var line = item.prev();
      if (!line.hasClass('line')) {
        line = line.prev();
      }

      // When you hover over the item, reveal the line
      item.on('mouseenter.reveal-line', function(event) {
        line.addClass('near');
      }).on('mouseleave.reveal-line', function(event) {
        line.removeClass('near');
      });
    });
  };

  Paper.prototype.applyHeaderEvents = function(headers) {
    var self = this;

    // Show edit form
    headers.prev().children('.configure-token').on('click', function(event) {
      var header = $(this).parent().next();
      var text = header.text();

      var inputForm = $('<input type="text" id="paper-edit-widget"></input>');
      inputForm.val(text.trim());

      inputForm.on('blur', function(event) {
        header.text(inputForm.val());

        /* Send update event */
        var page_index = self.pageIndex();
        var item_index = header.data('item-index');
        Occam.object.queuePushSet('pages.' + page_index + '.items.' + item_index + '.data', {
          "content": inputForm.val()
        });

        inputForm.replaceWith(header);
        header.prev().children('.configure-token').trigger('mouseleave');

        if (header.text().trim().length == 0) {
          self.removeItem(header);
        }
      });

      header.after(inputForm);
      header.detach();
      inputForm.focus();
    });

    // Show edit form when double clicking on paragraph text
    headers.on('dblclick', function(event) {
      $(this).prev().children('.configure-token').trigger('click');
    });
  };

  Paper.prototype.initializeWidget = function(widgets) {
    var self = this;

    var widget_card = self.page.find('.card.widget-configuration.template');

    widgets.each(function() {
      var widget_configuration = widget_card.clone().removeClass('template');
      $(this).parent().after(widget_configuration);
      widget_configuration.find('.auto-complete').each(function() {
        new Occam.AutoComplete($(this));
      });
      Occam.Tabs.bind(widget_configuration);
      Occam.Directory.bind(widget_configuration);
      var separator = $('<div class="card filled separator" aria-hidden="true"></div>');
      $(this).parent().after(separator);
    });
  };

  Paper.prototype.applyWidgetEvents = function(widgets) {
    var self = this;
    var iframe = widgets;

    // Widget object selection event
    var separatorCard     = widgets.parent().next();
    var configurationCard = widgets.parent().next().next();

    new Occam.Card(separatorCard);
    new Occam.Card(configurationCard);

    configurationCard.find('.card').each(function() {
      new Occam.Card($(this), true);
    });

    var configurationTabs = new Occam.Tabs(configurationCard.children('ul.tabs'));

    configurationCard.find('form.widget-selection input.button').on('click', function(event) {
      event.stopPropagation();
      event.preventDefault();

      // Remove old configuration tabs
      while(configurationTabs.tabCount() > 2) {
        configurationTabs.removeTab(3);
      }

      var object_id = $(this).parent().children('input.hidden[name=object-id]').val();

      var objectURL = "/" + object_id;

      $.getJSON(objectURL, function(data) {
        var object_revision = data['revision'];

        iframe.data('object-id', object_id);
        iframe.data('object-revision', object_revision);
        iframe.data('object-name', data['name']);
        iframe.data('object-type', data['type']);

        self.createWidgetConfigurationTabs(iframe, data, configurationTabs, object_id, object_revision);

        var widgetURL = "/" + object_id + "/" + object_revision + "/raw/" + data["file"];
        widgets.attr('aria-hidden', 'false');
        widgets.parent().children('.card').css({
          "display": "none"
        }).attr('aria-hidden', 'true');
        widgets.css({
          "display": "block"
        });
        widgets.attr('src', widgetURL);
      });
    });

    // Configuration updating events
    widgets.parent().find('.line .configure-token').on('click', function(event) {
      var configureToken = $(this);

      configureToken.toggleClass('shown');

      // Show the delete token
      var deleteToken = configureToken.parent().children('.delete-token');
      var newToken    = configureToken.parent().children('.new-token');
      configureToken.parent().toggleClass('shown');

      // Show the Configure section
      var card = configureToken.parents('.card');

      if (card.next().attr('aria-hidden') == 'true') {
        card.next().attr('aria-hidden', 'false');
        card.next().next().attr('aria-hidden', 'false');
      }
      else {
        card.next().attr('aria-hidden', 'true');
        card.next().next().attr('aria-hidden', 'true');
      }

      var iframe = card.children('iframe');

      var configurationData = {};

      // For each configuration, attach a change event
      card.next().next().find('li.tab-panel.configuration').each(function() {
        self.bindWidgetConfigurationEvent(configurationData, $(this), iframe);
      });
    });
  };

  Paper.prototype.createWidgetConfigurationTabs = function(iframe, objectInfo, configurationTabs, object_id, object_revision) {
    var self = this;

    // A reference that holds all configuration data
    var configurationData = {};

    self.bindWidgetMessageEvents(iframe, configurationData);

    if ('configurations' in objectInfo) {
      objectInfo['configurations'].forEach(function(configuration, i) {
        var name = configuration['name'];
        var label = name;
        if ('label' in configuration) {
          label = configuration['label'];
        }

        configurationTabs.addTab(configuration['label'], function(tabPanel) {
          tabPanel.addClass('configuration');
          tabPanel.data("object-revision", object_revision);
          tabPanel.data("object-id", object_id);
          tabPanel.data("configuration-index", i);
          tabPanel.data("key", btoa(name));

          self.bindWidgetConfigurationEvent(configurationData, tabPanel, iframe);
        })
      });
    }
  };

  Paper.prototype.bindWidgetConfigurationEvents = function(iframe, configurationData) {
    this.bindWidgetMessageEvents(iframe, configurationData);
  };

  Paper.prototype.bindWidgetConfigurationEvent = function(configurationData, tabPanel, iframe) {
    var self = this;

    // Load the configuration, and once loaded, amend the configuration data
    // into the saved configuration data for the widget
    // Whenever the configuration is updated, send the new data to the widget
    Occam.Configuration.load(tabPanel, function(configuration) {
      configurationData[configuration.label] = configuration.data();
      tabPanel._timerId = null;

      configuration.on('change', function(event) {
        configurationData[configuration.label] = event;

        if (tabPanel._timerId != null) {
          // Reset timer
          window.clearTimeout(tabPanel._timerId);
          tabPanel._timerId = null;
        }

        tabPanel._timerId = window.setTimeout(function() {
          tabPanel._timerId = null;

          // Save configuration data
          var pageIndex = self.pageIndex();
          var itemIndex = iframe.data('item-index');
          Occam.object.queuePushSet('pages.' + pageIndex + '.items.' + itemIndex + '.data', {
            'object': {
              'id':       iframe.data('object-id'),
              'revision': iframe.data('object-revision'),
              'name':     iframe.data('object-name'),
              'type':     iframe.data('object-type')
            },
            'height': iframe.parent().css('height'),
            'configuration': configurationData
          });
        }, Paper.SAVE_DELAY_MS);

        iframe[0].contentWindow.postMessage({
          name: 'updateConfiguration',
          data: configurationData
        }, '*');
      });
    });
  };

  /* Implements the widget API and binds it to the given iframe element.
   * Given also is a reference to the configurationData, which is maintained
   * with the widget's configuration options.
   */
  Paper.prototype.bindWidgetMessageEvents = function(iframe, configurationData) {
    window.addEventListener('message', function(event) {
      var message = event.data;

      // React if the source of the message is the iframe
      if (event.source === iframe[0].contentWindow) {
        if (message.name === 'updateData') {
          // Resolve data in configuration
          var objectId       = iframe.data('object-id');
          var objectRevision = iframe.data('object-revision');

          var url = "/" + objectId +
                    "/"         + objectRevision +
                    "/configurations/data";

          // POST to /<id>/<rev>/configurations/data to retrieve
          // datapoints for the requested data.
          $.ajax({
            type: "POST",
            url: url,
            data: JSON.stringify(message.data),
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            success: function(data) {
              message.datapoints = data;
              iframe[0].contentWindow.postMessage(message, '*');
            }
          }).fail(function(xhr, error) {
          });
        }
        else if (message.name === 'updateConfiguration') {
          // Resend configuration data
          message['data'] = configurationData;
          iframe[0].contentWindow.postMessage(message, '*');
        }
      }
    });

    // Post the configuration data to the widget
    iframe[0].contentWindow.postMessage({
      'name': 'updateConfiguration',
      'data': configurationData
    }, '*');
  };

  Paper.prototype.applyParagraphEvents = function(paragraphs) {
    var self = this;

    // Show edit form
    paragraphs.prev().children('.configure-token').on('click', function(event) {
      var paragraph = $(this).parent().next();
      var text = paragraph.text();

      var inputForm = $('<textarea id="paper-edit-widget"></textarea>');
      inputForm.text(text.trim());

      inputForm.on('blur', function(event) {
        var newText = inputForm.val();
        var card = $(this).parents(".card");
        var prevCard = card.prev();
        var page = $(this).parents(".tab-panel");

        // Split the text into sections based on double newlines
        newParagraphs = newText.split("\n\n");

        paragraph.text(newParagraphs[0]);

        /* Send update event */
        var pageIndex = self.pageIndex();
        var itemIndex = paragraph.data('item-index');
        if (newParagraphs[0].trim().length > 0) {
          Occam.object.queuePushSet('pages.' + pageIndex + '.items.' + itemIndex + '.data', {
            "content": newParagraphs[0]
          });
        }

        inputForm.replaceWith(paragraph);
        paragraph.prev().children('.configure-token').trigger('mouseleave');

        // Add new paragraph items afterward
        if (newParagraphs.length > 1) {
          var currentParagraph = paragraph;

          // Generate new cards for each paragraph
          for (var i = 1; i < newParagraphs.length; i++) {
            self.addComponentAfter("paragraph", itemIndex+i, card, currentParagraph, newParagraphs[i]);
            currentParagraph = currentParagraph.next().next();
          }
        }

        if (paragraph.text().trim().length == 0) {
          self.removeItem(paragraph);
        }
      });

      paragraph.after(inputForm);
      paragraph.detach();
      inputForm.focus();
    });

    // Show edit form when double clicking on paragraph text
    paragraphs.on('dblclick', function(event) {
      $(this).prev().children('.configure-token').trigger('click');
    });
  };

  Paper.prototype.applyAddEvent = function() {
    var self = this;

    self.page.find('.new.item .add-component .button').on('click', function(event) {
      event.stopPropagation();
      event.preventDefault();

      // Determine Type
      type = $(this).parent().children("select.component.selector").next().data('original-text').trim();

      // Get the new item section
      newItemCard = $(this).parents(".card.page");

      // Get the card to add this one to
      prevCard = newItemCard.prev();

      // For the last item, remove the new-item button
      var last = newItemCard.next().length == 0;
      var lastLine = null;
      if (last) {
        if (prevCard.length > 0) {
          lastLine = prevCard.children('.line:last-child');
          lastLine.remove();
        }
      }

      var itemIndex = $(this).data('item-index');

      if (itemIndex === null) {
        itemIndex = self.pageItemCount();
      }

      // Move card to bottom somewhere
      var page = newItemCard.parents('.tab-panel');
      newItemCard.remove();

      // Make new item section invisible
      newItemCard.attr('aria-hidden', 'true');

      // Add card after
      self.addComponent(type, itemIndex, page, prevCard);

      if (last) {
        var lastCard = page.children('.card.page:last-child');
        if (lastCard.length > 0) {
          lastCard.append(lastLine);
          self.applyTokenEvents(lastLine);
        }
      }

      page.append(newItemCard);
      self.applyAddEvent();
      self.applySelectorEvents();
    });
  };

  /* This method initializes the dropdown selectors.
   */
  Paper.prototype.applySelectorEvents = function() {
    Occam.Selector.load(this.page.find('select.selector.component')).bindEvents(true);
  };

  /* This method will download the paper description and apply the
   * configurations for each configuration field.
   */
  Paper.prototype.pullConfigurations = function() {
    var self = this;
    Occam.object.objectInfo(function(metadata) {
      metadata.pages[self.pageIndex()].items.forEach(function(item, i) {
        if (item.type === "widget") {
          var iframe = self.page.find('iframe.paper-widget[data-item-index=' + i + ']');

          var configurationData = item.data.configuration;

          self.bindWidgetConfigurationEvents(iframe, configurationData);
        }
      });
    })
  };
};
