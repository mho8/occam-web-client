/*
 * This module handles Tab strips on the site. It allows you to wrap a ul.tabs
 * element and provides functions to add a tab, set the current tab, etc.
 */

$(function() {
  // Initialize all Tab Strips
  Occam.Tabs.bind(document.body);
});

var initOccamTabs = function(Occam) {
  'use strict';

  /*
   * This constructor creates an object that represents a tabstrip. You pass in
   * the jQuery element for the tabstrip to attach the object to the given
   * element on the page. If more than one element are passed, the first is
   * used.
   */
  var Tabs = Occam.Tabs = function(element) {
    if (element) {
      this.element = element;
      this.panels = element.parentNode.querySelector(':scope > .tab-panels');
      if (!(this.panels)) {
        this.panels = element.parentNode.parentNode.querySelector(':scope > .tab-panels');
      }
      if (this.element.getAttribute('data-index') === undefined) {
        this.element.getAttribute('data-index', Tabs._count);
        Tabs._count++;
      }
    }
    else {
      throw("Requires at least one element to be passed");
    }
  };

  Tabs._count = 0;

  Tabs.bind = function(element) {
    element.querySelectorAll('ul.tabs').forEach(function(tabElement) {
      (new Occam.Tabs(tabElement)).bindEvents();
    });
  };

  Tabs.load = function(element_or_id) {
    var element = element_or_id;
    if (typeof element_or_id === "string" || typeof element_or_id === "number") {
      element = document.querySelector('ul.tabs[data-index=' + element_or_id + ']');
    }

    if (!element.matches('ul.tabs')) {
      element = element.querySelector('ul.tabs');
    }

    return (new Occam.Tabs(element)).bindEvents();
  };

  /*
   * This method is the event callback that will reveal the bound tab.
   */
  Tabs.revealTabEvent = function(event) {
    var self = Occam.Tabs.load(this.parentNode);

    // Get the index of the tab that invoked the event
    var index = getChildIndex(this);

    // Select that tab
    self.select(index);
  };

  Tabs.prototype.updateState = function(state) {
    this.select(state.index);
  };

  /*
   * This method returns the unique index for this tab strip.
   */
  Tabs.prototype.index = function() {
    return this.element.getAttribute('data-index');
  };

  /*
   * This function selects the given tab by its index. The first tab is at
   * index 0.
   */
  Tabs.prototype.select = function(index) {
    var oldIndex = this.selected();

    if (oldIndex == index) {
      return;
    }

    var tab = this.element.querySelector('.tab:nth-child(' + (index + 1) + ')');
    if (!tab) {
      return;
    }

    tab.parentNode.querySelectorAll(':scope > .active').forEach(function(item) {
      var link = item.querySelector("a");
      item.classList.remove('active');
      link.setAttribute("tabindex", "-1");
      link.setAttribute("aria-selected", "false");
    });

    var link = tab.querySelector('a');
    link.setAttribute("aria-selected", "true");
    link.setAttribute("tabindex", "0");
    tab.classList.add('active');

    // Hide tab
    var currentTab = this.panels.querySelector(':scope > .tab-panel.active');
    if (currentTab) {
      currentTab.classList.remove('active');
      currentTab.setAttribute('aria-hidden', 'true');
    }

    // Reveal tab
    var tabPanel = this.panels.querySelector(':scope > .tab-panel:nth-child(' + (index + 1) + ')');
    tabPanel.classList.add('active');
    tabPanel.setAttribute('aria-hidden', 'false');

    // Dynamic load, if necessary
    if (link.getAttribute('data-pjax') === "true") {
      link.getAttribute('data-pjax', 'complete');

      // Pull page data
      $.ajax({
        type: "GET",
        url: link.attr('href'),
        success: function(html) {
          tabPanel.removeClass('unloaded');
          tabPanel.html(html);

          if (tabPanel.hasClass('page')) {
            new Occam.Paper(tabPanel);
          };
        }
      }).fail(function(error) {
      }).always(function() {
      });
    }

    // Update the browser url
    if (this.element.parentNode.classList.contains('content')) {
      var state = {"tab": oldIndex, "html": "", "pageTitle": ""};
      Occam.NavigationState.pushState("Tabs", this.index(), {'index': oldIndex}, {'index': index}, tab.querySelector('a').getAttribute('href'), "");

      // AJAX Load Page

      // Fix card heights
      var tabPanel = this.tabPanelAt(index);
      if (tabPanel) {
        tabPanel.querySelectorAll('.card').forEach(function(card) {
          new Occam.Card(card);
        });
      }
    }
    
    return this;
  };

  /*
   * This method will return the index of the currently selected tab.
   */
  Tabs.prototype.selected = function() {
    var self = this;

    var currentTab = self.element.querySelector(':scope > li.tab.active');

    if (!currentTab) {
      return -1;
    }

    return getChildIndex(currentTab);
  };

  /*
   * This method will ensure that tab events are bound to this tabstrip. You
   * can give a jQuery tab element which will have its events bound individually
   * or, when called without arguments, the events will be bound to all tabs on
   * the strip.
   */
  Tabs.prototype.bindEvents = function(tab) {
    var self = this;
    if (tab) {
      tab.addEventListener('click', Tabs.revealTabEvent);
      var link = tab.querySelector(':scope > a');
      link.addEventListener('click', function(event) {
        event.preventDefault();
      });

      // Do not allow it to be tabbed to
      link.setAttribute("tabindex", "-1");

      // Allow the current tab to be tabbed to
      self.element.querySelector(':scope > li.active a').setAttribute("tabindex", "0");

      // Allow the navigation of the tabstrip via keyboard
      link.addEventListener("keydown", function(event) {
        var toggle = null;
        if (event.keyCode == 37) { // left arrow
          toggle = tab.previousElementSibling;
        }
        else if (event.keyCode == 39) { // right arrow
          toggle = tab.nextElementSibling;
        }
        else if (event.keyCode == 40) { // down arrow
          // Unfocus the tabstrip and go to content
          event.preventDefault();
          event.stopPropagation();

          var panel = self.tabPanelAt(self.selected());
          panel.setAttribute("tabindex", "0");
          panel.focus();
          panel.setAttribute("tabindex", "-1");

          return;
        }

        if (toggle) {
          var index = window.getChildIndex(toggle);
          self.select(index);
          toggle.querySelector("a").focus();
        }
      });
    }
    else {
      var self = this;
      self.element.querySelectorAll(':scope > li.tab').forEach(function(tab) {
        self.bindEvents(tab);
      });
    }

    return this;
  };

  /*
   * This method adds a tab at the given index with the given name. If no index
   * is given, the tab is appended to the end of the tabstrip (the right-hand
   * side assuming a left-to-right rendering)
   */
  Tabs.prototype.addTab = function(name, b, c) {
    var callback = b;
    var atIndex = b;

    if (arguments.length > 2) {
      callback = c;
    }
    else {
      atIndex = null;
    }

    var index = atIndex || this.element.children.length;
    if (atIndex == 0) {
      index = 0;
    }

    var tab      = document.createElement("li");
    tab.classList.add("tab");
    var link = document.createElement("a");
    link.textContent = name;
    tab.appendChild(link);

    var tabPanel = document.createElement("li");
    tabPanel.classList.add("tab-panel");

    this.element.appendChild(tab);
    this.panels.appendChild(tabPanel);

    this.bindEvents(tab);

    if (callback) {
      callback(tabPanel);
    }

    return this;
  };

  /*
   * This method removes the tab at the given index.
   */
  Tabs.prototype.removeTab = function(atIndex) {
    this.element.querySelector(':scope > .tab:nth-child(' + (atIndex + 1) + ')').remove();
    this.panels.querySelector(':scope > .tab-panel:nth-child(' + (atIndex + 1) + ')').remove();

    return this;
  };

  /*
   * This method returns the jQuery element of the tab at the given index.
   */
  Tabs.prototype.tabAt = function(atIndex) {
    return this.element.querySelector(':scope > .tab:nth-child(' + (atIndex + 1) + ')');
  };

  /*
   * This method returns the jQuery element of the tab-panel at the given index.
   */
  Tabs.prototype.tabPanelAt = function(atIndex) {
    return this.panels.querySelector(':scope > .tab-panel:nth-child(' + (atIndex + 1) + ')');
  };

  /* This method gives you the number of tabs
   */
  Tabs.prototype.tabCount = function() {
    return this.element.querySelectorAll(':scope > .tab').length;
  };
};

