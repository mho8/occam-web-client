/*
 * This is the main module for OCCAM's front end infrastructure. This file will
 * initialize the different modules and initiate some interactive elements on
 * the page.
 */

// The main OCCAM container
window.Occam = {};

// Flags
window.Occam.DEBUG = true;

window.Occam.Util = {};

function getParents(el, parentSelector /* optional */, filter /* optional */) {
  // If no parentSelector defined will bubble up all the way to *document*
  if (parentSelector === undefined) {
    parentSelector = document;
  }

  if (filter === undefined) {
    filter = false;
  }

  var parents = [];
  var p = el.parentNode;

  while (p && p.matches && !(p.matches(parentSelector))) {
    var o = p;
    if (!filter) {
      parents.unshift(o);
    }
    p = o.parentNode;
  }

  if (p.matches) {
    parents.unshift(p);
  }

  return parents;
}

function getChildIndex(el) {
  return (Array.prototype.indexOf.call(el.parentNode.children, el));
}

function getLastChild(el) {
  return (el.children[el.children.length - 1]);
}

function submitForm(form, data_or_callback, callback) {
  var data = data_or_callback;
  if (callback === undefined) {
    callback = data_or_callback;
    data = undefined;
  }

  var formdata = new FormData(form);

  if (data) {
    data.forEach(function(tuple) {
      formdata.set(tuple[0], tuple[1]);
    });
  }

  window.Occam.Util.ajax("POST", form.getAttribute("action"), formdata, callback)
}

window.Occam.Util.submitForm = submitForm;

window.Occam.Util.ajax = function(method, url, data, callback, responseType) {
  var oReq = new XMLHttpRequest();

  if (callback) {
    oReq.addEventListener("load", function(event) {
      if (oReq.readyState == 4 && oReq.status == 200) {
        var response = oReq.responseText;

        if (responseType == "application/json" || responseType == "json") {
          // TODO: handle errors
          response = JSON.parse(response);
        }

        callback(response);
      }
    });
  }

  oReq.open(method, url);
  oReq.setRequestHeader('X-Requested-With', 'XMLHttpRequest');

  if (responseType == "json" || responseType == "application/json") {
    oReq.setRequestHeader('Accept', 'application/json');
  }
  else if (responseType) {
    oReq.setRequestHeader('Accept', responseType);
  }

  if (data) {
    oReq.send(data);
  }
  else {
    oReq.send();
  }

  return oReq;
};

window.Occam.Util.post = function(url, data, callback, responseType) {
  var formdata = undefined;

  if (data) {
    formdata = new FormData();
    Object.keys(data).forEach(function(key) {
      formdata.set(key, data[key]);
    });
  }

  return window.Occam.Util.ajax("POST", url, formdata, callback, responseType);
}

window.Occam.Util.get = function(url, callback, responseType) {
  return window.Occam.Util.ajax("GET", url, null, callback, responseType);
}

function getParameterByName(name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, "\\$&");
  var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
  results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function copyToClipboard(text) {
    if (window.clipboardData && window.clipboardData.setData) {
        // IE specific code path to prevent textarea being shown while dialog is visible.
        return clipboardData.setData("Text", text); 

    } else if (document.queryCommandSupported && document.queryCommandSupported("copy")) {
        var textarea = document.createElement("textarea");
        textarea.textContent = text;
        textarea.style.position = "fixed";  // Prevent scrolling to bottom of page in MS Edge.
        document.body.appendChild(textarea);
        textarea.select();
        try {
            return document.execCommand("copy");  // Security exception may be thrown by some browsers.
        } catch (ex) {
            console.warn("Copy to clipboard failed.", ex);
            return false;
        } finally {
            document.body.removeChild(textarea);
        }
    }
}

// On-Load Code:
$(function() {
  var Occam = window.Occam;

  initOccamNavigationState(Occam);
  initOccamCard(Occam);
  initOccamObject(Occam);
  initOccamSocial(Occam);
  initOccamHelp(Occam);
  initOccamPermissions(Occam);
  initOccamModal(Occam);
  initOccamTooltip(Occam);
  initOccamObjectTree(Occam);
  initOccamDropdown(Occam);
  initOccamSelector(Occam);
  initOccamDirectory(Occam);
  initOccamAutoComplete(Occam);
  initOccamValidator(Occam);
  initOccamTabs(Occam);
  initOccamConfiguration(Occam);
  initOccamConfigurator(Occam);
  initOccamDataViewer(Occam);
  initOccamJob(Occam);
  initOccamRunList(Occam);
  initOccamRunViewer(Occam);
  initOccamRunTopbar(Occam);
  initOccamRunner(Occam);
  initOccamVT100(Occam);
  initOccamWebSocket(Occam);
  initOccamTerminal(Occam);
  initOccamImporter(Occam);
  initOccamPaper(Occam);
  initOccamWorkflow(Occam);

  var copyActions = document.querySelectorAll("a.copy");

  copyActions.forEach(function(element) {
    element.removeAttribute("hidden");

    // When we click on this, add the contents of the "data-value" to the clipboard
    element.addEventListener("click", function(event) {
      event.preventDefault();
      event.stopPropagation();
      copyToClipboard(window.location.protocol + "//" + window.location.host + element.getAttribute("data-url"));
    });
  });

  /* Permissions Pane */

  /* Object Header */
  var header = document.querySelector(".content > h1");

  // Bookmarks
  if (header) {
    var bookmarkToggle = header.querySelector("li.bookmark");
    if (bookmarkToggle) {
      var bookmarkDelete = header.querySelector("li.bookmark_delete");

      bookmarkToggle.addEventListener("click", function(event) {
        event.preventDefault();
        event.stopPropagation();

        var form = this.querySelector("form");

        Occam.Util.submitForm(form);

        this.setAttribute("hidden", "");
        bookmarkDelete.removeAttribute("hidden");
      });

      bookmarkDelete.addEventListener("click", function(event) {
        event.preventDefault();
        event.stopPropagation();

        var form = this.querySelector("form");

        Occam.Util.submitForm(form);

        this.setAttribute("hidden", "");
        bookmarkToggle.removeAttribute("hidden");
      });
    }
  }
});

$(function() {
  var nameEntry = $('.name#object-name');

  $(window).on('resize.update-text-fill', function(event) {
    nameEntry.children('span').css({'white-space': 'nowrap'});
    nameEntry.textfill({
      'widthOnly': true,
      'minFontPixels': 18,
      'fail': function() {
        nameEntry.children('span').css("font-size", "18px");
      },
      'complete': function() {
      }
    });
    if (nameEntry.children('span').css("font-size") == "18px") {
      nameEntry.children('span').css({'white-space': 'normal'});
    }
    nameEntry.css({'line-height': '1.3em', 'font-size': nameEntry.children('span').css('font-size')});
  }).trigger('resize.update-text-fill');
});

// Autocomplete for experiment tags
$(function(){
  var tag_cache = {};
  $('input.tagged').tagit({
    allowSpaces: true,
    singleField: true,
    singleFieldDelimiter: ';'
  });
  $('input.tagged.autocomplete').tagit({
    singleField: true,
    singleFieldDelimiter: ';',
    autocomplete: {
      delay: 0,
      minLength: 2,
      source: function(request, response) {
        var term = request.term;
        if (term in tag_cache) {
          response(tag_cache[term]);
          return;
        }

        $.getJSON($('input.tagged.autocomplete').data("source"), {term: term}, function(data, status, xhr) {
          tag_cache[term] = data;
          response(data);
        });
      },
    }
  });
  $('#search').searchlight('/search', {
    showIcons: false,
    align: 'left'
  });

  $('.configuration-group > li > .recipe > select').on('change', function(e) {
    var selected = $(this).children(':selected');
    var ul_group = $(this).parent().parent().children('ul.configuration-group');
    var expand = $(this).parent().parent().children('h2').children('span.expand');
    var nesting = ul_group.data('nesting');

    // Expand group
    if (!expand.hasClass('shown')) {
      expand.trigger('click');
    }

    // Ask for the config options
    jQuery.getJSON(selected.data('template'), function(data, status, xhr) {
      // Set all input fields
      $.each(data, function(key, value) {
        code = $.base64.encode(key);
        // Replace + / with - _
        code = code.replace('+', '-').replace('/', '_');
        // Remove padding
        while (code.charAt(code.length-1) == '=') {
          code = code.slice(0, code.length-1);
        }
        code = nesting + '[' + code + ']';

        $('*[name="'+code+'"]').each(function(e) {
          $(this).val(value);
        });
      });
    });
  });


  // add prettyprint class to all <pre><code></code></pre> blocks
  var prettify = false;
  $("pre code").parent().each(function() {
    $(this).addClass('prettyprint');
    prettify = true;
  });

  // if code blocks were found, bring in the prettifier ...
  if ( prettify ) {
    $.getScript("/js/prettify.js", function() { prettyPrint() });
  }
});
